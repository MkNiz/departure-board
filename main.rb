## [1] RESOURCES
require 'sinatra'
require 'csv'
require 'open-uri'
require 'active_support/core_ext/time'
# require 'pry'

## [2] CONSTANTS
# A constant that represents the URI of the departures CSV file
DEPARTURE_URI = "http://developer.mbta.com/lib/gtrtfs/Departures.csv"

## [3] FUNCTIONS
# This function parses a remote CSV file with headers, translating it into an
# array of hashes.
def import_csv(uri)
  data = Array.new
  file = CSV.parse(open(uri), headers: true, header_converters: :symbol).to_a
  headers = file.slice!(0)
  file.each do |row|
    row_hash = Hash.new
    row.each_with_index { |column, idx| row_hash.merge!({ headers[idx] => column }) }
    data << row_hash
  end
  # Test Entry for delays
  # data << {
  #   :timestamp => 60659, :origin => "North Station", :trip => "1999",
  #   :destination => "A Good Place", :scheduledtime => "1471116600",
  #   :lateness => "600", :status => "Delayed"
  # }
  data = translate_data(data)
end

# This function translates epoch times and seconds into formatted strings,
# and additionally adds useful fields that assist in constructing the view.
def translate_data(d)
  d.each do |item|
    time = Time.at(item[:scheduledtime].to_i).in_time_zone('Eastern Time (US & Canada)')
    item[:scheduledtime] = epoch_to_hm(item[:scheduledtime].to_i)
    item[:lateness_hms] = seconds_to_hms(item[:lateness].to_i)
    item[:status_str] = "#{item[:status]}"
    item[:tooltip] = "The #{item[:trip]} to #{item[:destination]} "
    item[:row_style] = ""
    safe_dest = item[:destination].gsub('/', '_')
    safe_dest.gsub!(/\s/,'%20')
    item[:url_safe_dest] = safe_dest
    if item[:status] == "Cancelled" then
      item[:row_style] = "background:rgba(128,0,0,0.25);"
      item[:tooltip] << "has been cancelled."
    elsif item[:status] == "Delayed" then
      item[:row_style] = "background:rgba(128,128,0,0.25);"
      item[:status_str] << "(#{item[:lateness_hms]})"
      item[:tooltip] << "has been delayed for #{item[:lateness_hms]}."
      expected_time = time + item[:lateness].to_i
      item[:expected_time] = expected_time.strftime("%I:%M %p")
    elsif item[:status] == "On Time"
      item[:tooltip] << "is on time and should arrive as scheduled."
    elsif item[:status] == "Now Boarding"
      item[:row_style] = "background:rgba(255,255,255,0.1);"
      item[:status_str] << ": Track #{item[:track]}"
      item[:tooltip] << "is now boarding on Track #{item[:track]}."
    elsif item[:status] == "All Aboard"
      item[:row_style] = "background:rgba(255,255,255,0.25);"
      item[:status_str] << ": Track #{item[:track]}"
      item[:tooltip] << "is about to leave Track #{item[:track]}."
    elsif item[:status] == "Departed"
      item[:row_style] = "background:rgba(0,0,0,0.5)"
      item[:tooltip] << "has departed."
    end
  end
  d
end

# This function returns the current time as a string
def current_time_to_s()
  Time.now.in_time_zone('Eastern Time (US & Canada)').strftime("%I:%M:%S %p %Z, %m/%d/%Y")
end

# This function extracts all entries in array of hashes "a" where "key" equals "val"
def get_where(a, key, val)
  new_array = Array.new
  a.each { |item| if item[key] == val then new_array << item end }
  new_array
end

# This function translates an epoch time into a h:m:s string
def epoch_to_hm(epoch)
  dt = Time.at(epoch).in_time_zone('Eastern Time (US & Canada)')
  hms = dt.strftime("%I:%M %p")
end

# This function translates a seconds value into a m, s string
def seconds_to_hms(secs)
  str = ""
  h, m, s = 0, 0, secs
  while s >= 3600 do
    h += 1
    s -= 3600
  end
  while s >= 60 do
    m += 1
    s -= 60
  end
  str << "#{h}h " if h>0
  str << "#{m}m " if m>0
  str << "#{s}s " if s>0
  str.strip!
end

## [4] INITIALIZATION
# Initializes data, sets haml options
departure_data = import_csv(DEPARTURE_URI)
last_refresh = current_time_to_s()
last_path = '/'
set :haml, :format => :html5

## [5] SINATRA ROUTING
get '/' do
  passed_tables = Array.new
  passed_tables[0] = { :name => "North Station", :t => get_where(departure_data, :origin, "North Station") }
  passed_tables[1] = { :name => "South Station", :t => get_where(departure_data, :origin, "South Station") }
  hasData = true
  hasData = false if passed_tables[0].length == 0 && passed_tables[1].length == 0
  last_path = '/'
  haml :index, locals: {
    :tables => passed_tables, :refresh => last_refresh,
    :page_title => "Departure Board - Both Stations", :hasData => hasData }
end

get '/refresh' do
  departure_data = import_csv(DEPARTURE_URI)
  last_refresh = current_time_to_s()
  redirect to(last_path)
end

get '/southstation' do
  passed_tables = Array.new
  passed_tables[0] = { :name => "South Station", :t => get_where(departure_data, :origin, "South Station") }
  hasData = true
  hasData = false if passed_tables[0].length == 0
  last_path = '/southstation'
  haml :index, locals: {
    :tables => passed_tables, :refresh => last_refresh,
    :page_title => "Departure Board - South Station", :hasData => hasData
  }
end

get '/northstation' do
  passed_tables = Array.new
  passed_tables[0] = { :name => "North Station", :t => get_where(departure_data, :origin, "North Station") }
  hasData = true
  hasData = false if passed_tables[0].length == 0
  last_path = '/northstation'
  haml :index, locals: {
    :tables => passed_tables, :refresh => last_refresh,
    :page_title => "Departure Board - North Station", :hasData => hasData
  }
end

get '/delays' do
  delayed_trains = get_where(departure_data, :status, "Delayed")
  last_path = '/delays'
  haml :delays, locals: {
    :delays => delayed_trains, :refresh => last_refresh,
    :page_title => "Departure Board - All Delays"
  }
end

get '/destination/:dest' do
  dest = params[:dest]
  dest.gsub!("_", "/")
  trains_to_dest = get_where(departure_data, :destination, dest)
  station = "?"
  hasData = true
  trains_to_dest.length == 0 ? hasData = false : station = trains_to_dest[0][:origin]
  passed_tables = Array.new
  passed_tables[0] = { :name => "Trains headed to #{dest} from #{station}", :t => trains_to_dest}
  safe_dest = dest.gsub("/", "_")
  last_path = "/destination/#{safe_dest}"
  haml :index, locals: {
    :tables => passed_tables, :refresh => last_refresh,
    :page_title => "Departure Board - #{dest}", :hasData => hasData
  }
end

get '/destination' do
  redirect to("/")
end
